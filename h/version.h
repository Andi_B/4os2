// Version info

#include "build.h"

#ifdef _PM
#define VER_MAJOR 3
#define VER_MINOR 10
#define VER_REVISION_PM ".PM Technology Preview"

#else
#define VER_MAJOR 3
#define VER_MINOR 10
#define VER_REVISION ".preAB"   // Must be string with no spaces - leading dot optional
// #define VER_REVISION ".1-shl"   // Must be string with no spaces - leading dot optional

#endif

