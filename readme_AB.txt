Enhancements made by Andreas Buchinger - OLD FILE, mostly merged into .inf file
--------------------------------------

- Window title is current directory by default. Of course, if own title is set
  with title or window command, this will be used instead. To set back default
  title (directory), use title /d. If you ever want a window title of "/d"
  (without quotes), use window "/d" (with quotes). Use title /c if you like the
  old 4os2 standard title (4os2 ...)

- Directoy change by typing in the directory name without cd is now possible
  even without leading backslash (\). Backslash on some keyboard layouts (like
  the german one) is not that easy to type.

  If you want the old behaviour, disable it with 4os2.ini file entry
  CDWithoutBackslash=0

  With CDWithoutBackslash=1 (default), directory change takes place only if
  no executable with the same name exist

  With CDWithoutBackslash=2 a directory change is made before checking for an
  external executable with the same name. Of course you can start such an
  executcable by typing the name with extension f.i. name.exe or name.cmd.

- Popup window position is now relative to current cursor postion (negative
  values places popup window below current cursor line)

  You can disable this (and revert to the old behaviour) with 4os2.ini file
  entry PopupWinPosRelative=No

- ver /r now shows build date/time

- Show correct file size for files >= 2GB with dir command

- Tuning of the byte summary displayed at the bottom of a directory listing can
  be made with NewByteDisp setting in 4os2.ini file.

  NewByteDisp=1 (default) adds a (xxxGB) info for better readability for large
  values. The allocated space is not shown by default any more.

  NewByteDisp=2 in addition to level 1 reenables the allocated size summary but
  on a new line instead the same as the bytes/files/dirs (it does not fit there
  any more with 80 character displays and large values)

  Use 4os2.ini file entry NewByteDisp=0 if you like the old behaviour.

- touch works on large files now (>=2GB)

20090827
- added wrapper for OSes with no LFS (pre Warp Server for e-bussines)
- fixed dir for >=4GB files
- changed EA size field to 7 chars (dir command)

20100118
- copy with files >2GB should work now

- ranges in various command should work correctly with files >2GB now, added g
  and G range specifier (/[s3G] means bigger than 3GByte)

20100205
- special treatment for type on devices (type alsahlp$, type ibms506$...), in
contrary to the standard OS/2 type command the 4os2 type command handles
'carriage return' (0x0D, <CR>) characters as 'line feed' in page mode (/P) and
currently ignores it on normal mode.
Advantage: with weird devices like alsahlp$ no text is lost as with standard
type which overwrittes lines immediatly when <CR> is encountered
Disadvantage: with weird devices like alsahlp$ in page mode (/P) when end of
line is marked with a <LF> followed by <CR> instead the usual <CR><LF>, this
will produce intermediate empty lines. Anyway you can always redirect output to
a file to see what the device really wants to tell you. Or use "copy alsahlp$
con:" to see it similar than with cmd.exe (lines are overwritten with <CR>)

20100306
- added ALT-F4 to exit 4os2

20100318
- increased buffer to 4096 which makes possible 'type wrnd32$', though it do not
work with parameters /L or /P cause wrnd32$ do not allow reading with a buffer
smaller than app. 4k and 4os2 reads single bytes in this mode - can not be fixed
without huge amount of work. There are some versions of alsahlp$ / alsa32$ which
do not like reading single bytes too. These versions will also not work with /P
or /L options.

2010xxxx
- make pmprintf build env var dependant and cleaned up TRACE messages
- CMDSIZE to 4096 for type on devices

201005xx
- clean up formating in 'old' files

20100520
- added bldlevel to .exes and .dll

20100812
- reworked make_buildlevel.cmd

20100701
- <CTRL><up/down> implementation

20110407
- Debug code to find out if WM_QUIT or similar is sent when shuting down

201111xx
- v3.08 fixed ticket#14 CDWithoutBackslash=2
- Changed back VER_REVISION handling (no influence on official builds)
- changed readme history - last changes at the top


ToDo:
-----
DONE - files >=4GB are not displayed with dir!
DONE - copy (concatenate) large files do not work
DONE - cleanup debug messages

DONE - fix large file support for copy command - 20090611
DONE - fix large file support for tree command - 20090828

DONE - 20090827 LONGLONG for search range (misc.c)

DONE - 20091216 - help does not work, f.i. 'help jfs' ???

DONE - 20100207 - type alsahlp$, ibms506$

DONE - 20100318 page_break_bin really needed? or same as page_break?

DONE - 20100409 type \dev\ibms506$: works with cmd.exe but not with 4os2
         type \dev\ibms506$ works with 4os2 but not with cmd.exe

DONE - 20100520 - cleanup filecmds.c 2246-2250 and 2242
         - cleanup comment FIXME in misc 801

20100520 - check 4os2 from command line boot (load DOSCALLS required?)

DONE - 20100520 - in .ipf add ALT-F4 in section 'Technical and Compatibility
enhancements' and Title /C /D in 'Command Changes'

DONE - 20110105 - in .ipf add CTRL up/down in section 'Command-Line Editing',
'Cursor Movement Keys:'

DONE - 20110105 - change in readme1st 'exes' and all changes by Ste.... (3.06
not correct)

DONE - 20110531 - update .inf to 3.07 (ticket #7)

20140102 - repair make_buildlevel.cmd for PM builds
20140102 - 4os2pm VER_REVISION.....


Build enviroment:
-----------------
- As I compile within VisualSlickEdit (<F5>), I invoke wmake from an command
  file called vs_make.cmd. It sets the watcom enviroment variables and touches
  msg.c to reflect the build date/time in ver /r message. Look at the end of
  this file if you want to have a clue how my EnvWatcom.cmd works.

Notes:
------
- with debug build, the jpos2dll.dll do not work (SYS1808 with beginlibpath)

- if you like to automatically close your 4os2 windows by xshutdown, use
  xshutdown option 'close non PM windows automatically'


My EnvWatcom.cmd:
-----------------
/*  Check if watcom header directory is already in INCLUDE enviroment variable
  and if its not in front of os2tk45 or ibmcpp3.65 header files, set it with
  setvars.cmd which was created during watcom installation */
HaveIncTK = ""

IncVar = value( 'include', , 'OS2ENVIRONMENT')
IncVar = TRANSLATE(IncVar)

/* say IncVar */

/* search the positions of the various header subdirectories */
/* this assumes the tk45 is installed in a directory called os2tk45 */
/* and IBMC++ in IBMCPP3.65 ... Change to match your paths */
pos45 = Pos('OS2TK45\H' , IncVar)
posWA = Pos('WATCOM\H' , IncVar)
pos365 = Pos('IBMCPP3.65\INCLUDE' ,IncVar)

/* check if WATCOM is in INCLUDE _before_ ibmcpp3.65\include
  and before os2tk45\h */
IF posWA >  0  THEN HaveIncTK = "TRUE"

IF pos365 > 0 THEN
    DO
    IF pos365 < posWA THEN HaveIncTK = "FALSE"
    END

IF pos45 > 0 THEN
    DO
    IF pos45 < posWA THEN HaveIncTK = "FALSE"
    END

/* say "os2tk45 " pos45
say "ibmcpp3.65 " pos365
say "watcom " posWA     */

IF HaveIncTK = "TRUE" THEN
    DO
    rc=charout(,'1b'x||'[36;1m')
    say 'Include to watcom\h already set'
    rc=charout(,'1b'x||'[0m')
    RETURN
    END

/* Using watcom enviroment instead of ibmcpp365 or os2tk45 include files */
rc=charout(,'1b'x||'[31;1m')
say 'Setting include to watcom\...'
rc=charout(,'1b'x||'[0m')
/* path to watcom installation directory - change to match your drive/path */
'call p:\watcom\setvars.cmd'
rc=charout(,'1b'x||'[36;1m')
say 'Include to watcom\h sucessfully set'
rc=charout(,'1b'x||'[0m')
