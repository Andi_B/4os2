cd debug
@REM set watcom enviroment variables if not set correctly
call envwatcom.cmd

@REM change time of msg.c to force compile (for correct date/time in ver /r)
call wtouch -i ..\c\msg.c

@REM set pmprintf enviroment to enable pmprintf/trace
@set pmprintf=1

@REM start wmake, -e for 'do not ask for file deletetion' on build failure
call wmake -e
cd ..
